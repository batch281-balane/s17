/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printDetails(){

		let fullName = prompt("Enter your full name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location: ");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);

	};

	printDetails();
	


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

	
*/

	//second function here:
	alert("Hi! Enter your top 5 favorite bands/musical artist");
	function printSecondFunction(){

		let favBand1 = "E-Heads";
		let favBand2 = "Kamikazee";
		let favBand3 = "PNE";
		let favBand4 = "Hale";
		let favBand5 = "Silent Sanctuary";

		console.log("1. " + favBand1);
		console.log("2. " + favBand2);
		console.log("3. " + favBand3);
		console.log("4. " + favBand4);
		console.log("5. " + favBand5);


	}

	printSecondFunction();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	alert("Hi! Enter your top 5 favorite movies of all time");
	function printThirdFunction(){
		let favMovie1 = "Black Berry";
		let rating1 = '97%';
		let favMovie2 = "Chile 76";
		let rating2 = '100%';
		let favMovie3 = "Unrest";
		let rating3 = "79%";
		let favMovie4 = "You can live forever";
		let rating4 = "88%";
		let favMovie5 = "Guardians of the Galaxy vol. 3";
		let rating5 = "81%";

		console.log("1. " + favMovie1);
		console.log("Rotten Tomatoes Rating: " + rating1);
		console.log("2. " + favMovie2);
		console.log("Rotten Tomatoes Rating: " + rating2);
		console.log("3. " + favMovie3);
		console.log("Rotten Tomatoes Rating: " + rating3);
		console.log("4. " + favMovie4);
		console.log("Rotten Tomatoes Rating: " + rating4);
		console.log("5. " + favMovie5);
		console.log("Rotten Tomatoes Rating: " + rating5);


	}

	printThirdFunction();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();

	alert("Hi! Please add the names of your friends.");
	// let Friends() = 
	function printUsers(){
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();
// console.log(friend1);
// console.log(friend2);