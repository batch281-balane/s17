// Function
/*
	syntax: 
		function functionName(){
			code block (statement)
		};
*/

function printName(){
	console.log ("My name is Jear");
};
printName();

// Function declaration vs expressions


// function declaration
function declaredFunction(){
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// function expression
let variableFunction = function(){
	console.log("Hello Again!");
};

variableFunction();

declaredFunction = function(){
	console.log("Update declaredFunction");
};

declaredFunction();

const constantFunc = function(){
	console.log("Initialized with const!");
};

constantFunc();

/*constantFunc = function(){
	console.log("cannot be re-aassigned")
}
constantFunc();*/


// Function scoping
/*
	scope is the accessibility (visibility) of variables
	
	JS Variables has 3 types of scope:
	1. Local/block scope
	2. Global scope
	3. Function scope
*/

{
	let localVar = "Alonzo Mattheo";

	console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function showNames(){
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// Nested Functions

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}

	nestedFunction();
}

myNewFunction();
// nestedFunction();

// Function and Global scoped variables

// Global scoped variable

let globalName = "Joy";

function myNewFunction2(){
	let nameInside = "Kenzo";

	console.log(globalName);
}

myNewFunction2();

// Using alert()

alert("Hello World");  
// This will run immediately when the page loads

function showSampleAlert(){
	alert("Hello,User!");
}

showSampleAlert();

// Using Prompt()

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);



function printWelcomeMessage(){

	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
};

printWelcomeMessage();


/*
Function Naming Conventions
	- Function names should be definitive of the task
	it will perform
	- Avoid generic names to avoid confusion within the code
	- Avoid pointless and inappropriate function names
	- Name you functions ff camel casing.
	- Do not use JS reserved keywords
*/

